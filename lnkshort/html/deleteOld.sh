#!/bin/bash

TODAY=$(date +%Y%m%d)

for searchdir in * ; do
  if [ -d $searchdir ] && [ -e $searchdir$DELTAG ] ; then
    TO_DELETE=$(cat $searchdir$DELTAG)
    if [ "$(date -d $TO_DELETE +%Y%m%d)" -le "$TODAY" ] ; then
      link=$(./findLink.sh $searchdir)
      rm -r $searchdir
      echo "DELETED shortlink at /$searchdir that ponited to $link" >&2
    fi
  fi
done
