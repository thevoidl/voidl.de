#! /bin/sh
# 1: long url
# 2: random string (location on this server)

url=$1
dir=$2
A="A"
DELETE_DATE=$(date +%Y%m%d -d +5days)
ALREADY_THERE="false"

###TODO: make own script out of this and concat protocol prefixes
for searchdir in * ; do
  if [ "$(./findLink.sh $searchdir)" = "$url" ]; then
    echo --------------------------------------------- >&2
    echo Found link to $url at $searchdir >&2
    echo --------------------------------------------- >&2
    dir=$searchdir
    ALREADY_THERE="true"
    echo $dir
  fi
done
 
if [ $ALREADY_THERE != "true" ]; then
  while [ -d $dir ]; do
    dir=$dir$A
  done

  echo --------------------------------------------- >&2
  echo creating a new link to $url >&2
  echo at $dir >&2
  echo --------------------------------------------- >&2

  mkdir $dir
  cp -r draft/index.html ./$dir/

  # Echoing $dir here -> using return string in php script
  sed -i -e "s|LONGURL|$url|g" "$dir/index.html" && echo "$dir"
fi

## Deletion tag
echo $DELETE_DATE > ./$dir$DELTAG

## Delete old ones
./deleteOld.sh
