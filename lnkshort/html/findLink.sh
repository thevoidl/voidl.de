#!/bin/sh
# Find target URL

if [ -d $1 ] && [ -e $1/index.html ]; then
  printf "$(grep 'window.location.href =' $1/index.html | sed 's/.*= //' | tr --delete '"')"
fi
