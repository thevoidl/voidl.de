<html>
<body>

<?php
function getRandomString($n) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }
  
    return $randomString;
}

function addHttps($url) {
    if (substr($url, 0, 4) == 'http')
    {
       return $url;
    }
    return 'https://'.$url;
}

$str = getRandomString(3);
$longurl = addHttps($_GET['longurl']);
$newLink = shell_exec("./makeShort.sh " . $longurl." ". $str);
?>

<p>Short-url for <?php echo $_GET["longurl"]; ?> is</p>
<p>
<?php
    if (empty($newLink) || (int)$newLink > 0)
    { $disp = 'not available... Contact webmaster ät voidl . de'; }
    else
    {
        $shortUrl = "https://s.voidl.de/".$newLink;
	$disp = "<b><a href =\"$shortUrl\">$shortUrl</a></b><br>This Link will last for 5 days";
    }
    echo $disp;
?>
</p>

</body>
</html>
