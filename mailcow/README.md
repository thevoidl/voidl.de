Mailcow config for nginx proxy
---

The two files `docker-compose.override.yml` and `docker-compose.yml` contain my local changes to the common mailcow config. These enable it's existance behind my nginx proxy.

`docker-compose.override.yml` is essential. This file ensures that your local changes don't conflict with mailcow's `update.sh`. Also important is that you copy all existing networks from the original docker-compose file to the override file.

# If you want to use this as template:
 - Check the [Mailcow Docs](https://mailcow.github.io/mailcow-dockerized-docs/i_u_m_install/) for installation
 - Move `docker-compose.override.yml` to your mailcow root
 - Change the *networks* and the *aliases* in the `override` file according to your domain. The rest should work out of the box (using the generated mailcow.conf) 
 - Edit three lines in `docker-compose.yml`: (Comment the portbindings for `nginx-mailcow`)

```
  nginx-mailcow:
     #ports:
     #- "${HTTPS_BIND:-0.0.0.0}:${HTTPS_PORT:-443}:${HTTPS_PORT:-443}"
     #- "${HTTP_BIND:-0.0.0.0}:${HTTP_PORT:-80}:${HTTP_PORT:-80}"
```
